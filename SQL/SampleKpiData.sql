EXEC Tools.AddOrganizationFocusArea 60, 4; -- Tynset
EXEC Tools.AddOrganizationFocusArea 71, 4; -- Innlandet
EXEC Tools.AddOrganizationFocusArea 15, 4; -- Fonna
EXEC Tools.AddOrganizationFocusArea 13, 4; -- Haugesund
EXEC Tools.AddOrganizationFocusArea 75, 4; -- Stord
EXEC Tools.AddOrganizationFocusArea 71, 4; -- Innlandet
EXEC Tools.AddOrganizationFocusArea 93, 4; -- Helse Midt RHF
EXEC Tools.AddOrganizationFocusArea 130, 4; -- Trondheim
EXEC Tools.AddOrganizationFocusArea 160, 4; -- Trondheim
EXEC Tools.AddOrganizationFocusArea 143, 4; -- Hammerfest
EXEC Tools.AddOrganizationFocusArea 144, 4; -- Finnmark
EXEC Tools.AddOrganizationFocusArea 137, 4; -- Kirkenes
EXEC Tools.AddOrganizationFocusArea 156, 4; -- Trondheim
EXEC Tools.AddOrganizationFocusArea 3, 4; -- Bod�

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 3, 64, '2020-03-07', 20, 20 ),
( 75, 64, '2020-03-08', 30, 40 ),
( 75, 64, '2020-03-09', 40, 60 ),
( 75, 64, '2020-03-10', 60, 90 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 3, 65, '2020-03-07', 0, 1 ),
( 3, 65, '2020-03-08', 1, 1 ),
( 3, 65, '2020-03-09', 1, 1 ),
( 3, 65, '2020-03-10', 2, 1 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 3, 65, '2020-03-07', 0, 1 ),
( 3, 65, '2020-03-08', 1, 1 ),
( 3, 65, '2020-03-09', 1, 1 ),
( 3, 65, '2020-03-10', 2, 1 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 156, 64, '2020-03-07', 20, 20 ),
( 156, 64, '2020-03-08', 30, 40 ),
( 156, 64, '2020-03-09', 40, 60 ),
( 156, 64, '2020-03-10', 60, 90 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 143, 64, '2020-03-07', 30, 20 ),
( 143, 64, '2020-03-08', 40, 20 ),
( 143, 64, '2020-03-09', 60, 20 ),
( 143, 64, '2020-03-10', 90, 20 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 137, 64, '2020-03-07', 32, 20 ),
( 137, 64, '2020-03-08', 42, 20 ),
( 137, 64, '2020-03-09', 62, 20 ),
( 137, 64, '2020-03-10', 92, 20 );


INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 29, 64, '2020-03-07', 20, 20 ),
( 29, 64, '2020-03-08', 30, 40 ),
( 29, 64, '2020-03-09', 40, 60 ),
( 29, 64, '2020-03-10', 60, 90 );


INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 21, 64, '2020-03-07', 0, 20 ),
( 21, 64, '2020-03-08', 0, 40 ),
( 21, 64, '2020-03-09', 0, 60 ),
( 21, 64, '2020-03-10', 0, 90 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 181, 64, '2020-03-07',80, 20 ),
( 181, 64, '2020-03-08', 90, 40 ),
( 181, 64, '2020-03-09', 95, 60 ),
( 181, 64, '2020-03-10', 100, 90 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 194, 64, '2020-03-07',82, 20 ),
( 194, 64, '2020-03-08', 91, 40 ),
( 194, 64, '2020-03-09', 93, 60 ),
( 194, 64, '2020-03-10', 98, 90 );


INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 193, 64, '2020-03-07',52, 20 ),
( 193, 64, '2020-03-08', 61, 40 ),
( 193, 64, '2020-03-09', 83, 60 ),
( 193, 64, '2020-03-10', 98, 90 );


INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 56, 64, '2020-03-07',52, 20 ),
( 56, 64, '2020-03-08', 61, 40 ),
( 56, 64, '2020-03-09', 83, 60 ),
( 56, 64, '2020-03-10', 98, 90 );

INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 60, 64, '2020-03-07',56, 2 ),
( 60, 64, '2020-03-08', 60, 4 ),
( 60, 64, '2020-03-09', 88, 6 ),
( 60, 64, '2020-03-10', 93, 9 );


INSERT INTO DataPoints ( OrgId, DatasetId, TimeAxis, KPI, n ) VALUES 
( 3, 64, '2020-03-07',52, 2 ),
( 3, 64, '2020-03-08', 61, 4 ),
( 3, 64, '2020-03-09', 85, 6 ),
( 3, 64, '2020-03-10', 88, 9 );

select * from DataPoints where DatasetId = 64 order by TimeAxis

update DataPoints set TimeAxis = '2020-03-08' WHERE TimeAxis = '2020-08-03'
update DataPoints set TimeAxis = '2020-03-07' WHERE TimeAxis = '2020-07-03'
update DataPoints set TimeAxis = '2020-03-09' WHERE TimeAxis = '2020-09-03'
update DataPoints set TimeAxis = '2020-03-10' WHERE TimeAxis = '2020-10-03'